const R = require('ramda')
const { Composer } = require('micro-bot')
const { pickOne, parseQuestion, prepareLeetcodeTable } = require('./utils')
const app = new Composer()

prepareLeetcodeTable()
.then((leetcodeTable) => {
  app.hears(/(hello)|(hi)/g, (ctx) => ctx.reply(`嗨! ${R.pathOr('', ['message', 'from', 'username'], ctx)}`))
  app.command('start', ({ from, reply }) => {
    console.log('start', from)
    return reply('Meow I\'m cena cat!')
  })
  app.command('pickone', ({ message, ...ctx }) => {
    console.log(JSON.stringify(message))
    const [command, ...argvs] = message.text.split(' ')
    const difficulty = (R.pathOr('Easy', [0], argvs)).toLowerCase()
    const pickedQuestion = pickOne(difficulty, leetcodeTable)
    const {
      url,
      questionTitle,
      acceptRate,
    } = parseQuestion(pickedQuestion)
    ctx.reply(`連結：${url}
題目：${questionTitle}
通過率：${acceptRate}
難度：${difficulty}
    `)
  })
})
module.exports = app
